## Description

Simulation types of waves on surface of an elastic solid body:
* [Love waves](https://en.wikipedia.org/wiki/Love_wave)
* [Rayleigh waves](https://en.wikipedia.org/wiki/Rayleigh_wave)
