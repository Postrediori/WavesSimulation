#pragma once

namespace WavesSimulation
{

    constexpr double InitialAmplitude = 1.5;
    constexpr double InitialVelocity = 2.5;
    constexpr double InitialPeriod = 2.5;
    constexpr double InitialDissipation = 5.0;

    constexpr float MaxVelocity = 5.0;
    constexpr float MinPeriod = 1.5;
    constexpr float MaxPeriod = 5.0;
    constexpr float MaxDissipation = 5.0;

    constexpr double GeometrySize = 10.0;
    constexpr int GeometryResolution = 10;
    const glm::vec4 GeometryOrigin{ -GeometrySize / 2.0, -GeometrySize / 2.0, -GeometrySize * 5.0 / 2.0 , 0. };

}
