#pragma once

namespace WavesSimulation
{
    struct SymLambWaveModel : public BaseWaveModel {
        SymLambWaveModel() = default;

        auto GetDisplacement(const glm::vec4& coord0, double timeFactor) const->glm::vec4;
    };
}
