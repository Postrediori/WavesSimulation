#pragma once

namespace WavesSimulation
{
    struct PressureWaveModel : public BaseWaveModel {
        PressureWaveModel() = default;

        auto GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4;

        double dissipationFactor{ 0.25 };
    };
}
