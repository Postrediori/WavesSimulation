#include "pch.h"
#include "GraphicsLog.h"
#include "GraphicsResource.h"
#include "ShaderUtils.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "RayleighWaveModel.h"
#include "LoveWaveModel.h"
#include "PressureWaveModel.h"
#include "ShearWaveModel.h"
#include "AsymLambWaveModel.h"
#include "SymLambWaveModel.h"
#include "WavesSimulation.h"


namespace WavesSimulation
{
    const std::array<GLfloat, 4> MeshColor = { .9f, .9f, .9f, 1.f };
    const std::array<GLfloat, 4> OutlineColor = { .1f, .1f, .1f, 1.f };

    const std::string VertexShaderSource=R"glsl(
#version 330 core
in vec4 a_position;
uniform mat4 u_projection;
uniform mat4 u_view;
void main(void) {
    gl_Position = u_projection * u_view * vec4(a_position.xyz, 1.);
}
)glsl";

    const std::string FragmentShaderSource=R"glsl(
#version 330 core
out vec4 fragColor;
uniform vec4 u_color;
void main(void) {
    fragColor=vec4(u_color.xyz, 1.);
}
)glsl";

    WavesSimulation::WavesSimulation() {
        waveModels.push_back({ 1, "Rayleigh", std::make_unique<RayleighWaveModel>() });
        waveModels.push_back({ 2, "Love", std::make_unique<LoveWaveModel>() });
        waveModels.push_back({ 3, "Pressure", std::make_unique<PressureWaveModel>() });
        waveModels.push_back({ 4, "Shear", std::make_unique<ShearWaveModel>() });
        waveModels.push_back({ 5, "Asymetrical Lamb", std::make_unique<AsymLambWaveModel>() });
        waveModels.push_back({ 6, "Symetrical Lamb", std::make_unique<SymLambWaveModel>() });
    }

    auto WavesSimulation::Init() -> int {
        // Init vertices and indices buffers
        if (this->InitBuffers() != 0) {
            LOGE << "Cannot create buffers";
            return EXIT_FAILURE;
        }

        // Load shader program
        this->program.reset(ShaderUtils::CreateProgramFromSource(VertexShaderSource, FragmentShaderSource));
        if (!this->program) {
            LOGE << "Cannot create shader program for waves simulation";
            return EXIT_FAILURE;
        }

        this->aPosition = glGetAttribLocation(program.get(), "a_position"); LOGOPENGLERROR();
        this->uProjection = glGetUniformLocation(program.get(), "u_projection"); LOGOPENGLERROR();
        this->uView = glGetUniformLocation(program.get(), "u_view"); LOGOPENGLERROR();
        this->uColor = glGetUniformLocation(program.get(), "u_color"); LOGOPENGLERROR();
        if (this->aPosition==-1 || this->uProjection == -1 || this->uView == -1 || this->uColor == -1) {
            LOGE << "Cannot properly setup shader program for waves simulation";
            return EXIT_FAILURE;
        }

        // Init vertex arrays
        if (this->InitVertexArrays() != 0) {
            LOGE << "Cannot create vertex arrays";
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }

    auto WavesSimulation::Render(const glm::mat4& view, const glm::mat4& projection) -> void {
        glUseProgram(this->program.get()); LOGOPENGLERROR();

        glUniformMatrix4fv(this->uProjection, 1, GL_FALSE, glm::value_ptr(projection)); LOGOPENGLERROR();
        glUniformMatrix4fv(this->uView, 1, GL_FALSE, glm::value_ptr(view)); LOGOPENGLERROR();

        //
        // Draw Faces
        //
        glPolygonOffset(1.f, 0.f); LOGOPENGLERROR();
        glEnable(GL_POLYGON_OFFSET_FILL); LOGOPENGLERROR();

        glUniform4fv(this->uColor, 1, MeshColor.data()); LOGOPENGLERROR();

        // Top face
        glBindVertexArray(this->topFaceVao.get()); LOGOPENGLERROR();
        glDrawElements(GL_TRIANGLES, this->cubeIndicesCount, GL_UNSIGNED_INT, nullptr); LOGOPENGLERROR();

        // Left face
        glBindVertexArray(this->leftFaceVao.get()); LOGOPENGLERROR();
        glDrawElements(GL_TRIANGLES, this->cubeIndicesCount, GL_UNSIGNED_INT, nullptr); LOGOPENGLERROR();

        // Front face
        glBindVertexArray(this->vao.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIndicesFrontVbo.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeFrontVbo.get()); LOGOPENGLERROR();
        glDrawElements(GL_TRIANGLES, this->cubeIndicesFrontCount, GL_UNSIGNED_INT, nullptr); LOGOPENGLERROR();

        //
        // Draw lines
        //
        glPolygonOffset(0.f, 0.f); LOGOPENGLERROR();
        glDisable(GL_POLYGON_OFFSET_FILL); LOGOPENGLERROR();

        glUniform4fv(this->uColor, 1, OutlineColor.data()); LOGOPENGLERROR();

        // Top face
        glBindVertexArray(this->topFaceLinesVao.get()); LOGOPENGLERROR();
        glDrawElements(GL_LINES, this->cubeOutlineIndicesCount, GL_UNSIGNED_INT, nullptr); LOGOPENGLERROR();

        // Left face
        glBindVertexArray(this->leftFaceLinesVao.get()); LOGOPENGLERROR();
        glDrawElements(GL_LINES, this->cubeOutlineIndicesCount, GL_UNSIGNED_INT, nullptr); LOGOPENGLERROR();

        // Front face
        glBindVertexArray(this->vao.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeOutlineIndicesFrontVbo.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeFrontVbo.get()); LOGOPENGLERROR();
        glDrawElements(GL_LINES, this->cubeOutlineIndicesFrontCount, GL_UNSIGNED_INT, nullptr); LOGOPENGLERROR();

        // Finish rendering
        glBindVertexArray(0); LOGOPENGLERROR();
        glUseProgram(0); LOGOPENGLERROR();
    }

    auto WavesSimulation::Update(double dt) -> void {
        simulationTimeFactor += velocity * dt;

        std::vector<glm::vec4> newCubeData;
        const auto& pWaveModel = std::get<2>(this->waveModels.at(this->currentWaveModelIdx));

        // Top
        newCubeData.reserve(GeometryResolution * 5 * GeometryResolution);
        for (int z = 0; z < GeometryResolution * 5; z++) {
            for (int x = 0; x < GeometryResolution; x++) {
                auto originalCoord = this->GetCoord(cubeDataTop, z, x);
                auto displacement = pWaveModel->GetDisplacement(originalCoord, simulationTimeFactor);

                newCubeData.push_back(originalCoord + displacement);
            }
        }
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeTopVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ARRAY_BUFFER, sizeof(newCubeData[0]) * newCubeData.size(),
            newCubeData.data(), GL_DYNAMIC_DRAW); LOGOPENGLERROR();

        // Left
        newCubeData.clear();
        newCubeData.reserve(GeometryResolution * 5 * GeometryResolution);
        for (int z = 0; z < GeometryResolution * 5; z++) {
            for (int y = 0; y < GeometryResolution; y++) {
                auto originalCoord = this->GetCoord(cubeDataLeft, z, y);
                auto displacement = pWaveModel->GetDisplacement(originalCoord, simulationTimeFactor);

                newCubeData.push_back(originalCoord + displacement);
            }
        }
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeLeftVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ARRAY_BUFFER, sizeof(newCubeData[0]) * newCubeData.size(),
            newCubeData.data(), GL_DYNAMIC_DRAW); LOGOPENGLERROR();


        // Top
        newCubeData.clear();
        newCubeData.reserve(GeometryResolution * GeometryResolution);
        for (int x = 0; x < GeometryResolution; x++) {
            for (int y = 0; y < GeometryResolution; y++) {
                auto originalCoord = this->GetCoord(cubeDataFront, x, y);
                auto displacement = pWaveModel->GetDisplacement(originalCoord, simulationTimeFactor);

                newCubeData.push_back(originalCoord + displacement);
            }
        }
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeFrontVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ARRAY_BUFFER, sizeof(newCubeData[0]) * newCubeData.size(),
            newCubeData.data(), GL_DYNAMIC_DRAW); LOGOPENGLERROR();

    }

    auto WavesSimulation::InitBuffers() -> int {
        //
        // Vertices
        //

        // Top face vertices
        cubeDataTop.reserve(GeometryResolution * 5 * GeometryResolution);
        for (int z = 0; z < GeometryResolution * 5; z++) {
            for (int x = 0; x < GeometryResolution; x++) {
                cubeDataTop.push_back({
                    (x * GeometrySize) / (GeometryResolution - 1) + GeometryOrigin.x,
                    GeometrySize + GeometryOrigin.y,
                    (z * GeometrySize * 5.0) / (GeometryResolution * 5 - 1) + GeometryOrigin.z,
                    0.f
                });
            }
        }

        // Left face vertices
        cubeDataLeft.reserve(GeometryResolution * 5 * GeometryResolution);
        for (int z = 0; z < GeometryResolution * 5; z++) {
            for (int y = 0; y < GeometryResolution; y++) {
                cubeDataLeft.push_back({
                    GeometryOrigin.x,
                    (y * GeometrySize) / (GeometryResolution - 1) + GeometryOrigin.y,
                    (z * GeometrySize * 5.0) / (GeometryResolution * 5 - 1) + GeometryOrigin.z,
                    0.f
                });
            }
        }

        // Front face vertices
        cubeDataFront.reserve(GeometryResolution * GeometryResolution);
        for (int x = 0; x < GeometryResolution; x++) {
            for (int y = 0; y < GeometryResolution; y++) {
                cubeDataFront.push_back({
                    (x * GeometrySize) / (GeometryResolution - 1) + GeometryOrigin.x,
                    (y * GeometrySize) / (GeometryResolution - 1) + GeometryOrigin.y,
                    GeometrySize*5 + GeometryOrigin.z,
                    0.f
                });
            }
        }

        //
        // Vertex buffers
        //
        // Top
        glGenBuffers(1, &this->cubeTopVbo); LOGOPENGLERROR();
        if (!this->cubeTopVbo) {
            LOGE << "Cannot create VBO for waves simulation";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeTopVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeDataTop[0]) * cubeDataTop.size(),
            cubeDataTop.data(), GL_DYNAMIC_DRAW); LOGOPENGLERROR();

        // Left
        glGenBuffers(1, &this->cubeLeftVbo); LOGOPENGLERROR();
        if (!this->cubeLeftVbo) {
            LOGE << "Cannot create VBO for waves simulation";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeLeftVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeDataLeft[0]) * cubeDataLeft.size(),
            cubeDataLeft.data(), GL_DYNAMIC_DRAW); LOGOPENGLERROR();

        // Front
        glGenBuffers(1, &this->cubeFrontVbo); LOGOPENGLERROR();
        if (!this->cubeFrontVbo) {
            LOGE << "Cannot create VBO for waves simulation";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeFrontVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeDataFront[0]) * cubeDataFront.size(),
            cubeDataFront.data(), GL_DYNAMIC_DRAW); LOGOPENGLERROR();


        //
        // Indices
        //

        // Top&Left
        std::vector<GLuint> cubeIndices;
        cubeIndices.reserve((GeometryResolution * 5 - 1) * (GeometryResolution - 1));
        for (int z = 0; z < GeometryResolution * 5 - 1; z++) {
            for (int x = 0; x < GeometryResolution - 1; x++) {
                int topLeft{ z * GeometryResolution + x },
                    topRight{ topLeft + 1 },
                    bottomLeft{ topLeft + GeometryResolution },
                    bottomRight{ bottomLeft + 1 };

                cubeIndices.push_back(topLeft);
                cubeIndices.push_back(bottomLeft);
                cubeIndices.push_back(bottomRight);

                cubeIndices.push_back(bottomRight);
                cubeIndices.push_back(topRight);
                cubeIndices.push_back(topLeft);
            }
        }

        this->cubeIndicesCount = cubeIndices.size();


        // Front
        std::vector<GLuint> cubeIndicesFront;
        cubeIndicesFront.reserve((GeometryResolution - 1) * (GeometryResolution - 1));
        for (int x = 0; x < GeometryResolution - 1; x++)
            for (int y = 0; y < GeometryResolution - 1; y++) {{
                int topLeft{ x * GeometryResolution + y },
                    topRight{ topLeft + 1 },
                    bottomLeft{ topLeft + GeometryResolution },
                    bottomRight{ bottomLeft + 1 };

                cubeIndicesFront.push_back(topLeft);
                cubeIndicesFront.push_back(bottomLeft);
                cubeIndicesFront.push_back(bottomRight);

                cubeIndicesFront.push_back(bottomRight);
                cubeIndicesFront.push_back(topRight);
                cubeIndicesFront.push_back(topLeft);
            }
        }

        this->cubeIndicesFrontCount = cubeIndicesFront.size();

        //
        // Index buffers
        //
        glGenBuffers(1, this->cubeIndicesVbo.put()); LOGOPENGLERROR();
        if (!this->cubeIndicesVbo) {
            LOGE << "Failed to create indices VBO";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIndicesVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndices[0]) * cubeIndices.size(),
            cubeIndices.data(), GL_STATIC_DRAW); LOGOPENGLERROR();

        glGenBuffers(1, this->cubeIndicesFrontVbo.put()); LOGOPENGLERROR();
        if (!this->cubeIndicesFrontVbo) {
            LOGE << "Failed to create indices VBO";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIndicesFrontVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndicesFront[0]) * cubeIndicesFront.size(),
            cubeIndicesFront.data(), GL_STATIC_DRAW); LOGOPENGLERROR();


        //
        // Line indices
        //

        // Top&Left
        std::vector<GLuint> cubeOutlineIndices;
        cubeOutlineIndices.reserve(GeometryResolution * (GeometryResolution * 5 - 1) +
            GeometryResolution * 5 * (GeometryResolution - 1));

        for (int x = 0; x < GeometryResolution; x++) {
            for (int z = 0; z < GeometryResolution * 5 - 1; z++) {
                int topIndex{ z * GeometryResolution + x },
                    bottomIndex{ topIndex + GeometryResolution };
                cubeOutlineIndices.push_back(topIndex);
                cubeOutlineIndices.push_back(bottomIndex);
            }
        }

        for (int z = 0; z < GeometryResolution * 5; z++) {
            for (int x = 0; x < GeometryResolution-1; x++) {
                int leftIndex{ z * GeometryResolution + x },
                    rightIndex{ leftIndex + 1 };
                cubeOutlineIndices.push_back(leftIndex);
                cubeOutlineIndices.push_back(rightIndex);
            }
        }

        this->cubeOutlineIndicesCount = cubeOutlineIndices.size();

        // Front
        std::vector<GLuint> cubeOutlineIndicesFront;
        cubeOutlineIndicesFront.reserve(GeometryResolution*(GeometryResolution-1)*2);

        for (int x = 0; x < GeometryResolution; x++) {
            for (int y = 0; y < GeometryResolution - 1; y++) {
                int topIndex{ y * GeometryResolution + x },
                    bottomIndex{ topIndex + GeometryResolution };
                cubeOutlineIndicesFront.push_back(topIndex);
                cubeOutlineIndicesFront.push_back(bottomIndex);
            }
        }

        for (int y = 0; y < GeometryResolution; y++) {
            for (int x = 0; x < GeometryResolution - 1; x++) {
                int leftIndex{ y * GeometryResolution + x },
                    rightIndex{ leftIndex + 1 };
                cubeOutlineIndicesFront.push_back(leftIndex);
                cubeOutlineIndicesFront.push_back(rightIndex);
            }
        }

        this->cubeOutlineIndicesFrontCount = cubeOutlineIndicesFront.size();


        //
        // Lines index buffers
        //
        glGenBuffers(1, this->cubeOutlineIndicesVbo.put()); LOGOPENGLERROR();
        if (!this->cubeOutlineIndicesVbo) {
            LOGE << "Failed to create indices VBO";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeOutlineIndicesVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeOutlineIndices[0]) * cubeOutlineIndices.size(),
            cubeOutlineIndices.data(), GL_STATIC_DRAW); LOGOPENGLERROR();

        glGenBuffers(1, this->cubeOutlineIndicesFrontVbo.put()); LOGOPENGLERROR();
        if (!this->cubeOutlineIndicesFrontVbo) {
            LOGE << "Failed to create indices VBO";
            return EXIT_FAILURE;
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeOutlineIndicesFrontVbo.get()); LOGOPENGLERROR();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeOutlineIndicesFront[0]) * cubeOutlineIndicesFront.size(),
            cubeOutlineIndicesFront.data(), GL_STATIC_DRAW); LOGOPENGLERROR();

        return 0;
    }

    auto WavesSimulation::InitVertexArrays() -> int {
        //
        // GenericVAO
        //
        glGenVertexArrays(1, this->vao.put()); LOGOPENGLERROR();
        if (!this->vao) {
            LOGE << "Cannot create VAO for waves simulation";
            return EXIT_FAILURE;
        }
        glBindVertexArray(vao.get()); LOGOPENGLERROR();

        glEnableVertexAttribArray(this->aPosition); LOGOPENGLERROR();
        glVertexAttribPointer(this->aPosition, 4, GL_FLOAT, GL_FALSE,
            sizeof(glm::vec4), static_cast<void *>(0)); LOGOPENGLERROR();

        //
        // Top face VAO
        //
        glGenVertexArrays(1, this->topFaceVao.put()); LOGOPENGLERROR();
        if (!this->topFaceVao) {
            LOGE << "Cannot create VAO for waves simulation";
            return EXIT_FAILURE;
        }

        glBindVertexArray(topFaceVao.get()); LOGOPENGLERROR();

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIndicesVbo.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeTopVbo.get()); LOGOPENGLERROR();

        glEnableVertexAttribArray(this->aPosition); LOGOPENGLERROR();
        glVertexAttribPointer(this->aPosition, 4, GL_FLOAT, GL_FALSE,
            sizeof(glm::vec4), static_cast<void *>(0)); LOGOPENGLERROR();

        //
        // Left face VAO
        //
        glGenVertexArrays(1, this->leftFaceVao.put()); LOGOPENGLERROR();
        if (!this->leftFaceVao) {
            LOGE << "Cannot create VAO for waves simulation";
            return EXIT_FAILURE;
        }

        glBindVertexArray(leftFaceVao.get()); LOGOPENGLERROR();

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeIndicesVbo.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ARRAY_BUFFER, this->cubeLeftVbo.get()); LOGOPENGLERROR();

        glEnableVertexAttribArray(this->aPosition); LOGOPENGLERROR();
        glVertexAttribPointer(this->aPosition, 4, GL_FLOAT, GL_FALSE,
            sizeof(glm::vec4), static_cast<void *>(0)); LOGOPENGLERROR();

        //
        // Top face lines VAO
        //
        glGenVertexArrays(1, this->topFaceLinesVao.put()); LOGOPENGLERROR();
        if (!this->topFaceLinesVao) {
            LOGE << "Cannot create VAO for waves simulation";
            return EXIT_FAILURE;
        }

        glBindVertexArray(topFaceLinesVao.get()); LOGOPENGLERROR();

        glBindBuffer(GL_ARRAY_BUFFER, this->cubeTopVbo.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeOutlineIndicesVbo.get()); LOGOPENGLERROR();

        glEnableVertexAttribArray(this->aPosition); LOGOPENGLERROR();
        glVertexAttribPointer(this->aPosition, 4, GL_FLOAT, GL_FALSE,
            sizeof(glm::vec4), static_cast<void *>(0)); LOGOPENGLERROR();

        //
        // Left face lines VAO
        //
        glGenVertexArrays(1, this->leftFaceLinesVao.put()); LOGOPENGLERROR();
        if (!this->leftFaceLinesVao) {
            LOGE << "Cannot create VAO for waves simulation";
            return EXIT_FAILURE;
        }

        glBindVertexArray(leftFaceLinesVao.get()); LOGOPENGLERROR();

        glBindBuffer(GL_ARRAY_BUFFER, this->cubeLeftVbo.get()); LOGOPENGLERROR();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->cubeOutlineIndicesVbo.get()); LOGOPENGLERROR();

        glEnableVertexAttribArray(this->aPosition); LOGOPENGLERROR();
        glVertexAttribPointer(this->aPosition, 4, GL_FLOAT, GL_FALSE,
            sizeof(glm::vec4), static_cast<void *>(0)); LOGOPENGLERROR();

        // Finish VAO init
        glBindVertexArray(0); LOGOPENGLERROR();

        return EXIT_SUCCESS;
    }

    auto WavesSimulation::GetCoord(const std::vector<glm::vec4>& data, int u, int v) -> glm::vec4 {
        size_t idx = u * GeometryResolution + v;
        return data.at(idx);
    }

    auto WavesSimulation::SetModel(int modelId) -> void {
        for (size_t i = 0; i < waveModels.size(); i++) {
            if (modelId == std::get<0>(waveModels.at(i))) {
                currentWaveModelIdx = i;
                return;
            }
        }
        LOGE << "Unable to find model with ID = " << modelId;
    }

    auto WavesSimulation::SetVelocity(double newVelocity) -> void {
        velocity = newVelocity;
    }

    auto WavesSimulation::SetPeriod(double period) -> void {
        for (auto& model : this->waveModels) {
            std::get<2>(model)->SetPeriod(period);
        }
    }

    auto WavesSimulation::SetDissipation(double dissipation) -> void {
        for (auto& model : this->waveModels) {
            std::get<2>(model)->SetDissipation(dissipation);
        }
    }

}
