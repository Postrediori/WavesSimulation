#include "pch.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "ShearWaveModel.h"

namespace WavesSimulation
{
    auto ShearWaveModel::GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4 {
        double distance{ coord0.z - GeometryOrigin.z };
        double delta{ distance / (GeometrySize * 5.0) };

        return glm::vec4{
            0.0,
            this->waveAmplitude *
                exp(-this->waveDissipation * delta * dissipationFactor) *
                cos(this->wavePeriod * distance - timeFactor),
            0.0, 0.0
        };
    }
}
