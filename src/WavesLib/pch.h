#pragma once

#include <array>
#include <cmath>
#include <memory>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <plog/Log.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
