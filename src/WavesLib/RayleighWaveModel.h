#pragma once

namespace WavesSimulation
{

    struct RayleighWaveModel : public BaseWaveModel {
        RayleighWaveModel() = default;

        auto GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4;

        // Mechanical parameters
        double E{ 1.0 }; // Imaginary material
        double rho{ 10.0 }; // Imaginary material
        double nu{ 0.3 }; // Let's stick to classics

        // Lame' parameters
        double lambda{ nu * E / ((1.0 + nu) * (1.0 - 2.0 * nu)) };
        double mu{ E / (2.0 * (1 + nu)) };

        // Approx. solution of Rayleigh wave
        double thetaR{ (0.87 + 1.12 * nu) / (1.0 + nu) };

        double timeFactorScale{ 0.5 }; // Make Rayleigh waves slower
    };

}
