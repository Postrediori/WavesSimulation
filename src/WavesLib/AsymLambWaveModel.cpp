#include "pch.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "AsymLambWaveModel.h"

namespace WavesSimulation
{
    auto AsymLambWaveModel::GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4 {
        double distance{ coord0.z - GeometryOrigin.z };
        double depth{ -GeometryOrigin.y - coord0.y };
        double delta{ distance / (GeometrySize * 5.0) };
        
        constexpr double LambDepthScale = 1. / 10.;

        double lambDistance = depth * LambDepthScale;
        double phi = this->wavePeriod * distance - timeFactor;
        double theta = sin(phi);

        return glm::vec4{
            0.0,
            lambDistance * cos(theta) + waveAmplitude * cos(phi),
            lambDistance * sin(theta),
            0.0
        };
    }
}
