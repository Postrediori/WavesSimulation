#pragma once

namespace WavesSimulation
{

    struct LoveWaveModel : public BaseWaveModel {
        LoveWaveModel() = default;

        auto GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4;
    };

}
