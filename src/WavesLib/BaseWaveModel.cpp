#include "pch.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"

namespace WavesSimulation
{

    auto BaseWaveModel::GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4 {
        return glm::vec4(0.f);
    }

    auto BaseWaveModel::SetPeriod(double newPeriod) -> void {
        this->wavePeriod = 1. / newPeriod;
    }

    auto BaseWaveModel::SetDissipation(double newDissipation) -> void {
        this->waveDissipation = newDissipation;
    }

}
