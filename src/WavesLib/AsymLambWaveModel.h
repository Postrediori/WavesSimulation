#pragma once

namespace WavesSimulation
{
    struct AsymLambWaveModel : public BaseWaveModel {
        AsymLambWaveModel() = default;

        auto GetDisplacement(const glm::vec4& coord0, double timeFactor) const->glm::vec4;
    };
}
