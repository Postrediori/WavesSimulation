#pragma once

namespace WavesSimulation
{
    using WaveModelInfo = std::tuple<int, std::string, BaseWaveModel::Pointer>;

    struct WavesSimulation {
        WavesSimulation();

        auto Init() -> int;
        auto Render(const glm::mat4& view, const glm::mat4& projection) -> void;

        auto Update(double dt) -> void;

        auto InitBuffers() -> int;
        auto InitVertexArrays() -> int;

        static auto GetCoord(const std::vector<glm::vec4>& data, int u, int v) -> glm::vec4;

        auto SetModel(int modelId) -> void;
        auto SetVelocity(double newVelocity) -> void;
        auto SetPeriod(double period) -> void;
        auto SetDissipation(double dissipation) -> void;

        GraphicsUtils::unique_program program;
        GLint aPosition{ -1 }, uProjection{ -1 }, uView{ -1 }, uColor{ -1 };

        GraphicsUtils::unique_vertex_array topFaceVao, topFaceLinesVao;
        GraphicsUtils::unique_vertex_array leftFaceVao, leftFaceLinesVao;
        GraphicsUtils::unique_vertex_array vao;

        GraphicsUtils::unique_buffer cubeTopVbo, cubeLeftVbo, cubeFrontVbo;
        GraphicsUtils::unique_buffer cubeIndicesVbo, cubeIndicesFrontVbo;
        GraphicsUtils::unique_buffer cubeOutlineIndicesVbo, cubeOutlineIndicesFrontVbo;
        GLsizei cubeIndicesCount{ 0 }, cubeIndicesFrontCount{ 0 };
        GLsizei cubeOutlineIndicesCount{ 0 }, cubeOutlineIndicesFrontCount{ 0 };

        double velocity{ InitialVelocity };
        double simulationTimeFactor{ 0.0 };

        size_t currentWaveModelIdx{ 0 };
        std::vector<WaveModelInfo> waveModels;

        std::vector<glm::vec4> cubeDataTop;
        std::vector<glm::vec4> cubeDataLeft;
        std::vector<glm::vec4> cubeDataFront;
    };

}
