#include "pch.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "SymLambWaveModel.h"

namespace WavesSimulation
{
    auto SymLambWaveModel::GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4 {
        double distance{ coord0.z - GeometryOrigin.z };
        double depth{ coord0.y };
        double delta{ distance / (GeometrySize * 5.0) };

        double lambDistance = depth / GeometrySize;
        double phi = this->wavePeriod * distance - timeFactor;

        return glm::vec4{
            0.0,
            waveAmplitude * lambDistance * cos(phi),
            waveAmplitude * cos(lambDistance) * sin(phi),
            0.0
        };
    }
}
