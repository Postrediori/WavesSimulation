#include "pch.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "RayleighWaveModel.h"

namespace WavesSimulation
{

    auto RayleighWaveModel::GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4 {
        double scale{ this->wavePeriod / 5.0 };
        double omega{ 2.5 };
        double A{ this->waveAmplitude * scale };

        glm::vec4 coord = coord0 * glm::vec4(scale, scale, scale*0.25, scale);

        // Depth of a point
        double depth{ GeometrySize / 2.0 * scale - coord.y };
        double delta{ depth / GeometrySize * this->waveDissipation * 2.0 };

        // Rayleigh wave numbers for longitudinal and transversal waves
        double cL{ omega * sqrt(rho / (lambda + 2.0 * mu)) };
        double cT{ omega * sqrt(rho / mu) };

        // Rayleigh wave velocity
        double c{ cT / sqrt(thetaR) };

        double qR{ sqrt(c * c - cL * cL) };
        double sR{ sqrt(c * c - cT * cT) };

        glm::vec3 amplitude{ 0.0, A * c, A * qR };

        glm::vec3 dissipation{
            0.0,
            exp(-qR * delta) - 2 * qR * sR / (cT * cT) * exp(-sR * delta),
            exp(-qR * delta) - 2 * c * c / (2 * c * c - cT * cT) * exp(-sR * delta)
        };

        double phi{ c * coord.z - omega * timeFactor * timeFactorScale };

        return glm::vec4(
            0.f,
            amplitude.y * dissipation.y * cos(phi - M_PI / 2.0),
            amplitude.z * dissipation.z * cos(phi),
            0.f);
    }

}
