#include "pch.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "LoveWaveModel.h"

namespace WavesSimulation
{

    auto LoveWaveModel::GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4 {
        double distance{ coord0.z - GeometryOrigin.z };
        double depth{ -GeometryOrigin.y - coord0.y };
        double delta{ depth / GeometrySize };

        return glm::vec4{
            this->waveAmplitude * exp(-this->waveDissipation * delta) *
                cos(this->wavePeriod * distance - timeFactor),
            0., 0., 0.
        };
    }

}
