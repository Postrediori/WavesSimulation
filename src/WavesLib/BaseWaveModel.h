#pragma once

namespace WavesSimulation
{

    struct BaseWaveModel {
        using Pointer = std::unique_ptr<BaseWaveModel>;

        BaseWaveModel() = default;
        virtual ~BaseWaveModel() {};

        virtual auto GetDisplacement(const glm::vec4& coord0, double timeFactor) const -> glm::vec4;

        auto SetPeriod(double newPeriod) -> void;
        auto SetDissipation(double newDissipation) -> void;

        double waveAmplitude{ InitialAmplitude };
        double wavePeriod{ 1. / InitialPeriod };
        double waveDissipation{ InitialDissipation };
    };

}
