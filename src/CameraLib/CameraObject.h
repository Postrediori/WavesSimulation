#pragma once

namespace Camera
{

    constexpr double CameraDistance = 50.f;

    constexpr double InitialAzimuth = 0.4;
    constexpr double InitialElevation = 0.5;

    constexpr double MinAzimuth = 0.25;
    constexpr double MaxAzimuth = 1.5;

    constexpr double MinElevation = 0.0;
    constexpr double MaxElevation = 1.25;

    struct CameraObject {
        auto IncrementAzimuth(double deltaAzimuth) -> void;
        auto IncrementElevation(double deltaElevation) -> void;

        auto GetViewMatrix() -> glm::mat4;

        auto MouseDown(double mouseX, double mouseY) -> void;
        auto MouseUp() -> void;
        auto MouseMove(double mouseX, double mouseY) -> void;

        auto Resize(int width, int height) -> void;

        double azimuth{ InitialAzimuth };
        double elevation{ InitialElevation };
        double cameraDistance{ CameraDistance };

        glm::mat4 viewMatrix;

        bool changed{ true };
        bool isCameraOrbiting{ false };
        double lastMouseX{ 0. }, lastMouseY{ 0. };

        int windowWidth{ 0 }, windowHeight{ 0 };
    };

}
