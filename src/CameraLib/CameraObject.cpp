#include "pch.h"
#include "CameraObject.h"

namespace Camera
{

    auto CameraObject::IncrementAzimuth(double deltaAzimuth) -> void {
        this->azimuth += deltaAzimuth;
        this->azimuth = std::clamp(this->azimuth, MinAzimuth, MaxAzimuth);
        this->changed = true;
    }

    auto CameraObject::IncrementElevation(double deltaElevation) -> void {
        this->elevation += deltaElevation;
        this->elevation = std::clamp(this->elevation, MinElevation, MaxElevation);
        this->changed = true;
    }

    auto CameraObject::GetViewMatrix() -> glm::mat4 {
        if (this->changed) {
            const glm::vec3 OrbitPoint{ 0., 0., 0. };
            const glm::vec3 DirectionX{ 1., 0., 0. };
            const glm::vec3 DirectionY{ 0., 1., 0. };

            glm::mat4 distanceTranslationMatrix = glm::translate(glm::mat4(1.),
                glm::vec3(0., 0., -this->cameraDistance));
            glm::mat4 orbitTranslationMatrix = glm::translate(glm::mat4(1.), -OrbitPoint);
            glm::mat4 xRotationMatrix = glm::rotate(glm::mat4(1.), static_cast<float>(this->elevation), DirectionX);
            glm::mat4 yRotationMatrix = glm::rotate(glm::mat4(1.), static_cast<float>(this->azimuth), DirectionY);

            this->viewMatrix = distanceTranslationMatrix * xRotationMatrix *
                yRotationMatrix * orbitTranslationMatrix;

            this->changed = false;
        }

        return this->viewMatrix;
    }

    auto CameraObject::MouseDown(double mouseX, double mouseY) -> void {
        this->isCameraOrbiting = true;
        this->lastMouseX = mouseX;
        this->lastMouseY = mouseY;
    }

    auto CameraObject::MouseUp() -> void {
        this->isCameraOrbiting = false;
    }

    auto CameraObject::MouseMove(double mouseX, double mouseY) -> void {
        if (this->isCameraOrbiting) {
            constexpr double OrbitingSensitivity = 4.0;

            this->IncrementAzimuth((mouseX - this->lastMouseX) / this->windowWidth * OrbitingSensitivity);
            this->IncrementElevation((mouseY - this->lastMouseY) / this->windowHeight * OrbitingSensitivity);
            this->lastMouseX = mouseX;
            this->lastMouseY = mouseY;

            this->changed = true;
        }
    }

    auto CameraObject::Resize(int width, int height) -> void {
        this->windowWidth = width;
        this->windowHeight = height;
    }

}
