#pragma once

namespace GraphicsUtils {

    struct ImGuiWrapper {
        ImGuiWrapper() = default;
        ~ImGuiWrapper();
        
        auto Init(GLFWwindow* window) -> int;
        auto Release() -> void;
        
        auto StartFrame() -> void;
        auto Render() -> void;
    };

}
