#include "pch.h"
#include "ImGuiWrapper.h"

namespace GraphicsUtils {

ImGuiWrapper::~ImGuiWrapper() {
    Release();
}

auto ImGuiWrapper::Init(GLFWwindow* window) -> int {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.IniFilename = nullptr; // Disable .ini

    static const std::string ImguiGlslVersion = "#version 330 core";
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(ImguiGlslVersion.c_str());

    // Setup of ImGui visual style
    ImGui::StyleColorsDark();
    ImGuiStyle& style = ImGui::GetStyle();
    style.WindowRounding = 0.0f;
    style.WindowBorderSize = 0.0f;
    
    return 0;
}

auto ImGuiWrapper::Release() -> void {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
}

auto ImGuiWrapper::StartFrame() -> void {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

auto ImGuiWrapper::Render() -> void {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

} // namespace GraphicsUtils
