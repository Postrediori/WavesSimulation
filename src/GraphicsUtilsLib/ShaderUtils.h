#pragma once

namespace ShaderUtils
{

    auto CreateProgramFromFiles(
        const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath) -> GLuint;

    auto CreateProgramFromSource(
        const std::string& vertexShaderSource, const std::string& fragmentShaderSource) -> GLuint;

}
