#pragma once

namespace GraphicsUtils {

    struct GlfwWrapper {
        GlfwWrapper() = default;
        ~GlfwWrapper();
        
        auto Init(const std::string& title, int width, int height) -> int;
        auto Release() -> void;
        
        auto GetWindow() const -> GLFWwindow*;
        
        static auto ErrorCallback(int error, const char* description) -> void;
        
        GLFWwindow* window_{ nullptr };
        
        int savedXPos_{ 0 }, savedYPos_{ 0 };
        int savedWidth_{ 0 }, savedHeight_{ 0 };
    };

}
