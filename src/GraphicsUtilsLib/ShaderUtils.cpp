#include "pch.h"
#include "GraphicsLog.h"
#include "ShaderUtils.h"

namespace ShaderUtils
{

    auto LoadFile(const std::string& filePath) -> std::string {
        std::ifstream in(filePath, std::ios::in);
        if (!in) {
            return "";
        }

        std::string line;
        std::stringstream str;
        while (std::getline(in, line)) {
            str << line << std::endl;
        }

        return str.str();
    }

    auto GetShaderInfo(GLuint shader) -> std::string {
        int length{0};

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length); LOGOPENGLERROR();
        if (length == 0) {
            return "";
        }

        std::vector<char> buffer(length);
        glGetShaderInfoLog(shader, length, nullptr, buffer.data()); LOGOPENGLERROR();

        std::string str(buffer.begin(), buffer.end());
        return str;
    }

    auto GetProgramInfo(GLuint program) -> std::string {
        int length{0};

        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length); LOGOPENGLERROR();
        if (length == 0) {
            return "";
        }

        std::vector<char> buffer(length);
        glGetProgramInfoLog(program, length, nullptr, buffer.data()); LOGOPENGLERROR();

        std::string str(buffer.begin(), buffer.end());
        return str;
    }

    auto ReleaseProgram(GLuint program, GLuint vertex, GLuint fragment) -> void {
        if (vertex) {
            glDeleteShader(vertex); LOGOPENGLERROR();
        }

        if (fragment) {
            glDeleteShader(fragment); LOGOPENGLERROR();
        }

        if (program) {
            glDeleteProgram(program); LOGOPENGLERROR();
        }
    }

    auto CreateProgramFromFiles(
        const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath) -> GLuint {
        LOGI << "Shader Files: " << vertexShaderFilePath << " " << fragmentShaderFilePath;

        std::string strVert{LoadFile(vertexShaderFilePath)};
        if (strVert.empty()) {
            LOGE << "Cannot load file " << vertexShaderFilePath;
            return 0;
        }

        std::string strFrag{LoadFile(fragmentShaderFilePath)};
        if (strFrag.empty()) {
            LOGE << "Cannot load file " << fragmentShaderFilePath;
            return 0;
        }

        return CreateProgramFromSource(strVert, strFrag);
    }

    auto CreateProgramFromSource(
        const std::string& vertexShaderSource, const std::string& fragmentShaderSource) -> GLuint {
                
        LOGD << "Vertex Shader    : " << vertexShaderSource.length() << " characters";
        LOGD << "Fragment Shader  : " << fragmentShaderSource.length() << " characters";

        GLint result{0};
        GLuint vShader{0}, fShader{0};
        GLuint sProgram{0};
        const GLchar* vertexSource = vertexShaderSource.c_str();
        const GLchar* fragmentSource = fragmentShaderSource.c_str();

        vShader = glCreateShader(GL_VERTEX_SHADER); LOGOPENGLERROR();
        if (!vShader) {
            LOGE << "Unable to Create Vertex Shader";
            goto error;
        }

        fShader = glCreateShader(GL_FRAGMENT_SHADER); LOGOPENGLERROR();
        if (!fShader) {
            LOGE << "Unable to Create Fragment Shader";
            goto error;
        }

        glShaderSource(vShader, 1, &vertexSource, nullptr); LOGOPENGLERROR();
        glCompileShader(vShader); LOGOPENGLERROR();
        glGetShaderiv(vShader, GL_COMPILE_STATUS, &result); LOGOPENGLERROR();
        if (!result) {
            LOGE << "Vertex Shader Error : " << GetShaderInfo(vShader);
            goto error;
        }

        glShaderSource(fShader, 1, &fragmentSource, nullptr); LOGOPENGLERROR();
        glCompileShader(fShader); LOGOPENGLERROR();
        glGetShaderiv(fShader, GL_COMPILE_STATUS, &result); LOGOPENGLERROR();
        if (!result) {
            LOGE << "Fragment Shader Error : " << GetShaderInfo(fShader);
            goto error;
        }

        sProgram = glCreateProgram(); LOGOPENGLERROR();
        if (!sProgram) {
            LOGE << "Unable to Create Program";
            goto error;
        }

        glAttachShader(sProgram, vShader); LOGOPENGLERROR();
        glAttachShader(sProgram, fShader); LOGOPENGLERROR();

        glLinkProgram(sProgram); LOGOPENGLERROR();

        glGetProgramiv(sProgram, GL_LINK_STATUS, &result); LOGOPENGLERROR();
        if (!result) {
            LOGE << "Linking Shader Error : " << GetProgramInfo(sProgram);
            goto error;
        }

        glDeleteShader(vShader); LOGOPENGLERROR();
        glDeleteShader(fShader); LOGOPENGLERROR();

        return sProgram;

error:
        ReleaseProgram(sProgram, vShader, fShader);

        return 0;
    }

}
