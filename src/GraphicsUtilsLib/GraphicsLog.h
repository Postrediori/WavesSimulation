#pragma once

#ifdef NDEBUG
# define LOGOPENGLERROR()
#else
# define LOGOPENGLERROR() GraphicsUtils::LogOpenGLError(__FILE__,__LINE__)
#endif

namespace GraphicsUtils
{
    auto LogOpenGLError(const char *file, int line) -> void;
}
