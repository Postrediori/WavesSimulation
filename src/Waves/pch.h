#pragma once

#include <cstdlib>
#include <iostream>
#include <exception>
#include <string>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <imgui.h>

#include <plog/Log.h>
#include <plog/Init.h>
#include <plog/Appenders/ConsoleAppender.h>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_clip_space.hpp> // glm::ortho
#include <glm/gtc/matrix_transform.hpp> // glm::rotate
