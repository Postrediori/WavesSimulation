#include "pch.h"
#include "LogFormatter.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "GraphicsResource.h"
#include "WavesSimulation.h"
#include "CameraObject.h"
#include "GlfwWrapper.h"
#include "ImGuiWrapper.h"
#include "WavesContext.h"

auto main(int argc, const char* argv[]) -> int {
    try {
        plog::ConsoleAppender<plog::LogFormatter> logger;
#ifdef NDEBUG
        plog::init(plog::info, &logger);
#else
        plog::init(plog::debug, &logger);
#endif

        Waves::WavesContext wavesContext;
        if (wavesContext.Init(argc, argv) != 0) {
            LOGE << "Cannot init context";
            return EXIT_FAILURE;
        }

        wavesContext.RunLoop();
    }
    catch (const std::exception& ex) {
        LOGE << ex.what();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
