#include "pch.h"
#include "GraphicsLog.h"
#include "GraphicsResource.h"
#include "SimulationParameters.h"
#include "BaseWaveModel.h"
#include "RayleighWaveModel.h"
#include "LoveWaveModel.h"
#include "WavesSimulation.h"
#include "CameraObject.h"
#include "GlfwWrapper.h"
#include "ImGuiWrapper.h"
#include "WavesContext.h"

namespace Waves
{
    constexpr int WindowWidth = 1280;
    constexpr int WindowHeight = 720;
    const std::string WindowTitle = "Waves Simulations";

    constexpr int GlVersionMajor = 3;
    constexpr int GlVersionMinor = 3;

    constexpr float Fov = 60. / 180. * 3.1416926;
    constexpr float MinAspect = 16. / 9.;
    constexpr float Near = 1.;
    constexpr float Far = 1000.;

    const std::array<GLfloat, 4> ClearColor = { .75f, .75f, .75f, 1.f };

    auto WavesContext::Init(int /*argc*/, const char* /*argv*/[]) -> int {
        if (glfwWrapper.Init(WindowTitle, WindowWidth, WindowHeight) != 0) {
            LOGE << "Cannot load GLFW";
            return EXIT_FAILURE;
        }

        glfwSwapInterval(0);

        RegisterCallbacks();

        imguiWrapper.Init(glfwWrapper.GetWindow());

        if (this->InitContext() != 0) {
            LOGE << "Cannot init context";
            return EXIT_FAILURE;
        }

        // First call of reshape
        this->ReshapeCallback(WindowWidth, WindowHeight);

        return EXIT_SUCCESS;
    }

    auto WavesContext::RunLoop() -> void {
        while (!glfwWindowShouldClose(glfwWrapper.GetWindow())) {
            glfwPollEvents();

            // Start ImGui frame
            imguiWrapper.StartFrame();

            this->Render();

            // Render ImGui
            imguiWrapper.Render();

            this->Update(glfwGetTime());

            glfwSwapBuffers(glfwWrapper.GetWindow());
        }
    }

    auto WavesContext::InitContext() -> int {
        LOGI << "OpenGL Renderer : " << glGetString(GL_RENDERER);
        LOGI << "OpenGL Vendor : " << glGetString(GL_VENDOR);
        LOGI << "OpenGL Version : " << glGetString(GL_VERSION);
        LOGI << "GLSL Version : " << glGetString(GL_SHADING_LANGUAGE_VERSION);

        if (this->wavesSimulation.Init() != 0) {
            LOGE << "Cannot init waves simulation";
            return EXIT_FAILURE;
        }

        if (wavesSimulation.waveModels.size() == 0) {
            LOGE << "No models found";
            return EXIT_FAILURE;
        }

        // Set the first model as active
        currentWaveModelId = std::get<0>(wavesSimulation.waveModels.at(0));

        this->projection = glm::perspective(Fov, MinAspect, Near, Far);

        this->camera.Resize(this->windowWidth, this->windowHeight);

        glClearDepth(1.0); LOGOPENGLERROR();
        glEnable(GL_DEPTH_TEST); LOGOPENGLERROR();

        glEnable(GL_MULTISAMPLE); LOGOPENGLERROR();

        glClearColor(ClearColor[0], ClearColor[1], ClearColor[2], ClearColor[3]); LOGOPENGLERROR();

        return EXIT_SUCCESS;
    }

    auto WavesContext::Render() -> void {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); LOGOPENGLERROR();
    
        this->wavesSimulation.Render(this->camera.GetViewMatrix(), this->projection);
        
        if (this->isShowUi) {
            this->RenderUi();
        }
    }

    auto WavesContext::Update(double t) -> void {
        double currentTime = t;
        double dt = currentTime - this->lastUpdateTime;
        this->lastUpdateTime = currentTime;

        this->wavesSimulation.Update(dt);

        if (currentTime - this->lastFpsTime > 1.0) {
            // Update FPS each second
            this->fpsCounter = ImGui::GetIO().Framerate;
            this->lastFpsTime = currentTime;
        }
    }

    auto WavesContext::RenderUi() -> void {
        constexpr int UiMargin = 10;
        const ImVec2 UiSize = ImVec2(300, 325);

        ImGui::SetNextWindowPos(ImVec2(UiMargin, this->windowHeight - UiSize.y - UiMargin), ImGuiCond_Always);
        ImGui::SetNextWindowSize(UiSize, ImGuiCond_Always);

        ImGui::Begin(WindowTitle.c_str(), nullptr,
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

        ImGui::Text("Wave model:");

        for (const auto& model : wavesSimulation.waveModels) {
            std::string name{ std::get<1>(model) };
            int modelId{ std::get<0>(model) };

            if (ImGui::RadioButton(name.c_str(), &currentWaveModelId, modelId)) {
                currentWaveModelId = modelId;
                this->wavesSimulation.SetModel(modelId);
            }
        }

        ImGui::Separator();

        if (ImGui::SliderFloat("Velocity", &currentVelocity, 0., WavesSimulation::MaxVelocity, "%.1f")) {
            this->wavesSimulation.SetVelocity(currentVelocity);
        }

        if (ImGui::SliderFloat("Period", &currentPeriod, WavesSimulation::MinPeriod, WavesSimulation::MaxPeriod, "%.1f")) {
            this->wavesSimulation.SetPeriod(currentPeriod);
        }

        if (ImGui::SliderFloat("Dissipation", &currentDissipation, 0., WavesSimulation::MaxDissipation, "%.1f")) {
            this->wavesSimulation.SetDissipation(currentDissipation);
        }

        ImGui::Separator();

        ImGui::Text("User Guide:");
        ImGui::BulletText("F1 to on/off fullscreen mode.");
        ImGui::BulletText("F2 to show/hide UI.");

        ImGui::Separator();

        ImGui::Text("FPS Counter: %.1f", this->fpsCounter);

        ImGui::End();
    }

    auto WavesContext::RegisterCallbacks() -> void {
        const auto window = glfwWrapper.GetWindow();

        glfwSetWindowUserPointer(window, static_cast<void*>(this));

        glfwSetKeyCallback(window, WavesContext::GlfwKeyboardCallback);
        glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

        glfwSetMouseButtonCallback(window, WavesContext::GlfwMouseCallback);
        glfwSetCursorPosCallback(window, WavesContext::GlfwMousePosCallback);
        glfwSetWindowSizeCallback(window, WavesContext::GlfwReshapeCallback);
    }

    auto WavesContext::KeyboardCallback(int key, int /*scancode*/, int action, int /*mods*/) -> void {
        static struct ScreenState {
            bool isFullscreen{ false };
            int savedXPos{ 0 }, savedYPos{ 0 };
            int savedWidth{ 0 }, savedHeight{ 0 };

            auto ToggleFullscreen(GLFWwindow* window) -> void {
                isFullscreen = !isFullscreen;
                if (isFullscreen) {
                    glfwGetWindowPos(window, &savedXPos, &savedYPos);
                    glfwGetWindowSize(window, &savedWidth, &savedHeight);

                    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
                    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
                    glfwSetWindowMonitor(window, monitor, 0, 0,
                        mode->width, mode->height, mode->refreshRate);
                    glfwSwapInterval(1);
                }
                else {
                    glfwSetWindowMonitor(window, nullptr,
                        savedXPos, savedYPos,
                        savedWidth, savedHeight, GLFW_DONT_CARE);
                    glfwSwapInterval(0);
                }
            }
        } gScreenState;

        if (action == GLFW_PRESS) {
            switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(glfwWrapper.GetWindow(), GLFW_TRUE);
                break;

            case GLFW_KEY_F1:
                gScreenState.ToggleFullscreen(glfwWrapper.GetWindow());
                break;

            case GLFW_KEY_F2:
                this->isShowUi = !this->isShowUi;
                break;
            }
        }
    }

    auto WavesContext::MouseCallback(int button, int action, int /*mods*/) -> void {
        if (button == GLFW_MOUSE_BUTTON_1) {
            if (action == GLFW_PRESS) {
                if (!ImGui::IsAnyItemHovered()) {
                    double mouseX{ 0. }, mouseY{ 0. };
                    glfwGetCursorPos(glfwWrapper.GetWindow(), &mouseX, &mouseY);
                    this->camera.MouseDown(mouseX, mouseY);
                }
            }
            else if (action == GLFW_RELEASE) {
                this->camera.MouseUp();
            }
        }
    }

    auto WavesContext::MousePosCallback(double mouseX, double mouseY) -> void {
        this->camera.MouseMove(mouseX, mouseY);
    }

    auto WavesContext::ReshapeCallback(int width, int height) -> void {
        glViewport(0, 0, width, height); LOGOPENGLERROR();
        
        this->windowWidth = width;
        this->windowHeight = height;

        if (height == 0) {
            height = 1;
        }

        float ratio = static_cast<float>(width) / static_cast<float>(height);
        this->projection = glm::perspective(Fov, ratio, Near, Far);

        this->camera.Resize(width, height);
    }

    auto WavesContext::GlfwKeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods) -> void {
        void* p = glfwGetWindowUserPointer(window);
        assert(p);
        WavesContext* wavesContext = static_cast<WavesContext *>(p);
        wavesContext->KeyboardCallback(key, scancode, action, mods);
    }

    auto WavesContext::GlfwMouseCallback(GLFWwindow* window, int button, int action, int mods) -> void {
        void* p = glfwGetWindowUserPointer(window);
        assert(p);
        WavesContext* wavesContext = static_cast<WavesContext *>(p);
        wavesContext->MouseCallback(button, action, mods);
    }

    auto WavesContext::GlfwMousePosCallback(GLFWwindow* window, double x, double y) -> void {
        void* p = glfwGetWindowUserPointer(window);
        assert(p);
        WavesContext* wavesContext = static_cast<WavesContext *>(p);
        wavesContext->MousePosCallback(x, y);
    }

    auto WavesContext::GlfwReshapeCallback(GLFWwindow* window, int width, int height) -> void {
        void* p = glfwGetWindowUserPointer(window);
        assert(p);
        WavesContext* wavesContext = static_cast<WavesContext *>(p);
        wavesContext->ReshapeCallback(width, height);
    }

}
