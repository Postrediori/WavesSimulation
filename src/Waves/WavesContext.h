#pragma once

namespace Waves {

    struct WavesContext {
        WavesContext() = default;

        auto Init(int argc, const char* argv[]) -> int;
        auto RunLoop() -> void;

        auto InitContext() -> int;

        auto Render() -> void;
        auto Update(double t) -> void;

        auto RenderUi() -> void;

        auto RegisterCallbacks() -> void;

        auto KeyboardCallback(int key, int scancode, int action, int mods) -> void;
        auto MouseCallback(int button, int action, int mods) -> void;
        auto MousePosCallback(double x, double y) -> void;
        auto ReshapeCallback(int width, int height) -> void;

        static auto GlfwKeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods) -> void;
        static auto GlfwMouseCallback(GLFWwindow* window, int button, int action, int mods) -> void;
        static auto GlfwMousePosCallback(GLFWwindow* window, double x, double y) -> void;
        static auto GlfwReshapeCallback(GLFWwindow* window, int width, int height) -> void;

        GraphicsUtils::GlfwWrapper glfwWrapper;
        GraphicsUtils::ImGuiWrapper imguiWrapper;

        int windowWidth{0}, windowHeight{0};

        bool isShowUi{true};

        double lastUpdateTime{0.0};
        double lastFpsTime{0.0};
        double fpsCounter{0.0};
        double rotationAngle{0.0};

        glm::mat4 view, projection;

        int currentWaveModelId{ 0 };
        float currentVelocity{ WavesSimulation::InitialVelocity };
        float currentPeriod{ WavesSimulation::InitialPeriod };
        float currentDissipation{ WavesSimulation::InitialDissipation };
        WavesSimulation::WavesSimulation wavesSimulation;
        Camera::CameraObject camera;
    };

}